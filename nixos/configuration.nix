# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
{
  inputs,
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # If you want to use overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
    };
  };

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
  nix.registry = (lib.mapAttrs (_: flake: {inherit flake;})) ((lib.filterAttrs (_: lib.isType "flake")) inputs);

  # This will additionally add your inputs to the system's legacy channels
  # Making legacy nix commands consistent as well, awesome!
  nix.nixPath = ["/etc/nix/path"];
  environment.etc =
    lib.mapAttrs'
    (name: value: {
      name = "nix/path/${name}";
      value.source = value.flake;
    })
    config.nix.registry;

  nix.settings = {
    # Enable flakes and new 'nix' command
    experimental-features = "nix-command flakes";
    # Deduplicate and optimize nix store
    auto-optimise-store = true;  
  };
  

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.extraModulePackages = [ pkgs.linuxPackages_latest.v4l2loopback ];
  
  #System
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.supportedFilesystems = [ "ntfs" ];
  boot.tmp.useTmpfs = true;
  networking.hostName = "alps"; # Define your hostname.
  networking.networkmanager.enable = true;
  time.timeZone = "Europe/Berlin";
  services.fstrim.enable = true;

  #TPM2 for virtual machine
  security.tpm2.enable = true;
  security.tpm2.pkcs11.enable = true;  # expose /run/current-system/sw/lib/libtpm2_pkcs11.so
  security.tpm2.tctiEnvironment.enable = true;  # TPM2TOOLS_TCTI and TPM2_PKCS11_TCTI env variables


  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    keyMap = "dvorak";
  };
  i18n.inputMethod = {
    enable = true;
    type = "fcitx5";
    fcitx5 = {
      plasma6Support = true;
      waylandFrontend = true;
      addons = with pkgs; [ fcitx5-unikey ];
    };
  };
  
  #Services 
  services.dbus.packages = [ pkgs.gcr ];
  programs.dconf.enable = true;
  services.udisks2.enable = true;
  services.gvfs = {
    enable = true;
    package = lib.mkForce pkgs.gnome.gvfs;
  };
  services.tlp.enable = true;
  services.geoclue2.enable = true;
  location.provider = "geoclue2";
  services.avahi.enable = true;
  services.usbmuxd.enable = true;

  # Graphics:  

  # OpenGL
  hardware.graphics.enable = true;
  
  #Nvidia 
  services.xserver.videoDrivers=["nvidia"];  
  hardware.nvidia = {
    modesetting.enable = true;
    powerManagement.enable = true;
    powerManagement.finegrained = true;
    open = true; 
    package = config.boot.kernelPackages.nvidiaPackages.beta;
  };
  nixpkgs.config.cudaSupport = true;
    
  #Offloading
  hardware.nvidia.prime = {
    intelBusId = "PCI:1:0:0";
    nvidiaBusId = "PCI:0:2:0";
    offload = {
      enable = true;
      enableOffloadCmd = true;
    };
  };
       
  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;
  
  #Virtualization
  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;
  virtualisation.docker.enable = true;

  # Sound
  security.rtkit.enable = true;
  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
      extraConfig.pipewire.adjust-sample-rate = {
        "context.properties" = {
          "default.clock.rate" = 192000;
          "defautlt.allowed-rates" = [ 192000 48000 44100 ];
          #"default.clock.quantum" = 32;
          #"default.clock.min-quantum" = 32;
          #"default.clock.max-quantum" = 32;
        };
      };
   };
  };

  # Bluetooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = false;
  services.blueman.enable = true;

  users.users.dan = {
     isNormalUser = true;
     extraGroups = [ "wheel"  "networkmanager" "libvirtd" "tss"]; # Enable ‘sudo’ for the user.
  }; 
  nix.settings.trusted-users = ["dan"];

  # Apps
  programs.thunar.enable = true;
  programs.thunar.plugins = with pkgs.xfce; [
    thunar-volman
  ];
  programs.nix-ld.enable = true;

  programs.steam.enable = true;
  programs.gamescope.enable = true;
  
  programs.command-not-found.enable = true;

  
  xdg.portal.enable = true;
  xdg.portal.extraPortals = [pkgs.xdg-desktop-portal-hyprland pkgs.xdg-desktop-portal-gtk];
  xdg.portal.config.common.default = "hyprland";
  
  #Flatpaks
  services.flatpak.enable = true;
  services.flatpak.update.onActivation = true;
  services.flatpak.packages = [{appId = "org.blender.Blender"; origin="flathub";}];

  # Networking 
  networking.nftables.enable = true;
  networking.firewall = {
    enable = false;
    allowedTCPPorts = [ 51417 ];
  };

  services.mullvad-vpn.enable = true;
  services.mullvad-vpn.package = pkgs.mullvad-vpn;

  #services.cloudflare-warp.enable = true;

  environment.systemPackages = with pkgs; [ gtk3 libimobiledevice
  ifuse # optional, to mount using 'ifuse'
 ];
  environment.variables = rec {
    GSETTINGS_SCHEMA_DIR="${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}/glib-2.0/schemas";
  };
  
  system.stateVersion = "23.11";
}
