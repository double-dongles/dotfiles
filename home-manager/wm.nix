{config, pkgs, ...}: 

{
  # WM
  wayland.windowManager.hyprland.enable = true;
  wayland.windowManager.hyprland.settings = {
    monitor = ", prefered, auto, 1.6";
    xwayland = {
      force_zero_scaling = true;
    };
    env= [
      "QT_QPA_PLATFORM,wayland"
      "QT_QPA_PLATFORMTHEME,qtct"
      "QT_WAYLAND_DISABLE_WINDOWDECORATION,1"
      "QT_AUTO_SCREEN_SCALE_FACTOR,1"
      "QT_STYLE_OVERRIDE,kvantum" 
    ];
    cursor = {
      no_hardware_cursors = true;
    };
    "$mod" = "SUPER";
    "$modShift" = "SUPER_Shift";    
    input = {
      kb_layout=["us"];
      kb_variant=["dvorak-intl"];
      touchpad = {
        natural_scroll = true;
      };
    };
    exec-once = [
      "swaybg -i $HOME/Pictures/background.jpg"
      "[workspace 10 silent] transmission-gtk"
      "[workspace 3 silent] vesktop --enable-wayland-ime --wayland-text-input-version=3"
      "[workspace 4 silent] steam -silent -forcedesktopscaling 1.4"
      "dex -a"
    ];
    general = {border_size = 0;};
    decoration = {rounding = 6;};
    dwindle = {
      force_split = 2;
    };
    windowrule = [
      "float, title:^(Learn OpenGL)$"
      "opacity 0.92 0.80, ^(.*)$"
      "opacity 1.00 0.80, ^(firefox)$"
    ];
    windowrulev2 = [
      "opacity 0.0 override, class:^(xwaylandvideobridge)$"
      "noanim, class:^(xwaylandvideobridge)$"
      "noinitialfocus, class:^(xwaylandvideobridge)$"
      "maxsize 1 1, class:^(xwaylandvideobridge)$"
      "noblur, class:^(xwaylandvideobridge)$"
      "nofocus, class:^(xwaylandvideobridge)$"
    ];
    # Repeating held 
    binde = [
      ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+"
      ", XF86AudioLowerVolume, exec, wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%-"
      ", XF86MonBrightnessUp, exec, brightnessctl set 1%+"
      ", XF86MonBrightnessDown, exec, brightnessctl s 1%-"
    ];
    # With mouse
    bindm = [
      "$mod, mouse:272, movewindow"
      "$mod, mouse:273, resizewindow"];
    
    # Pure kb
    bind = [
      #Application
      "$mod, F, fullscreen"
      "$mod, Return, exec, foot"
      "$mod, R, exec, fuzzel"
      "$mod, L, exec, thunar"

      # Print screen
      "$mod, Print, exec, grim -g \"$(slurp)\""

      # Audio
      ",XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle "
      
      #Closing application
      "$mod, Q, killactive"
      
      # Navigation
      "$mod, left, movefocus, l"
      "$mod, right, movefocus, r"
      "$mod, up, movefocus, u"
      "$mod, down, movefocus, d"

      # Move windows      
      "$modShift, left, movewindow, l"
      "$modShift, right, movewindow, r"
      "$modShift, up, movewindow, u"
      "$modShift, down, movewindow, d"
    ]      
    ++ (
      builtins.concatLists(builtins.genList(
        x: let
          ws = let
            c = (x + 1) / 10;
          in
            builtins.toString(x + 1 - (c * 10));
        in [
          "$mod, ${ws}, workspace, ${toString (x+1)}"
          "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
        ])
      10)
    );

    misc = {
      disable_hyprland_logo = true;
      disable_splash_rendering = true;
    };
  };
  programs.fuzzel = {
    enable = true;
    settings = {
      main = {
        font = "Cozette:size=16";
      };
      colors = {
        background = "2E3440DD";
        text = "ECEFF4FF";
        match = "88C0D0FF";
        selection = "4C566AFF";
        selection-text = "ECEFF4FF";
        selection-match = "88C0D0FF";
        border = "2E3440FF";
      };};
  };

  programs.waybar = {
    enable = true;
    systemd.enable = true;
    settings = {
      mainBar = {
        layer = "top";
        position = "top";
        height = 30;
        output = [
          "eDP-1"
          "HDMI-A-1"
        ];
        modules-left = [ "hyprland/workspaces" "hyprland/window"];
        modules-center = [];
        modules-right = ["pulseaudio" "backlight" "network" "cpu" "memory" "battery" "clock" "tray"];
        "hyprland/workspaces" = {
          format = "{icon}";
          disable-scroll = true;
          all-outputs = true;
        };
        "network" = {
          "interface" = "wlo1";
          "format" = "{ifname}";
          "format-wifi" = "{essid} ({signalStrength}%) ";
          "format-ethernet" = "{ipaddr}/{cidr} 󰊗";
          "format-disconnected" = ""; #An empty format will hide the module.
          "tooltip-format" = "{ifname} via {gwaddr} 󰊗";
          "tooltip-format-wifi" = "{essid} ({signalStrength}%) ";
          "tooltip-format-ethernet" = "{ifname} ";
          "tooltip-format-disconnected" = "Disconnected";
          "max-length" = 50;
        };
        "battery" ={
          "interval" =60;
          "states" ={
            "warning" =30;
            "critical" =15;
          };
          "format" ="{icon}  {capacity}%";
          "format-icons" =["" "" "" "" ""];
          "max-length" =25;
        };
        "pulseaudio" = {        
        "format" = "{icon}  {volume}%      {format_source}";
        "format-bluetooth" = "{volume}% {icon} {format_source}";
        "format-bluetooth-muted" = "{icon}  {format_source}";
        "format-source" = "  {volume}%";
        "format-source-muted" = "";
        "format-icons" = {
            "headphone" = "";
            "hands-free" = "";
            "headset" = "";
            "default" = ["" "" ""];
        };
        "on-click" = "pavucontrol";
        };
        "tray" = {
          "spacing" = 10;
        };        
        "cpu" = {
          "format" = "󰊚  {usage}%";
          "tooltip" = false;
        };
        "memory" = {
          "format" = "󰒋  {}%";
        };
        "backlight" = {
          "format" = "{icon}  {percent}%";
          "format-icons" = ["" ""];
        };      
      };
    };
    style = ''
      @define-color nord0 #2e3440;
@define-color nord1 #3b4252;
@define-color nord2 #434c5e;
@define-color nord3 #4c566a;
/* Snow storm */
@define-color nord4 #d8dee9;
@define-color nord5 #e5e9f0;
@define-color nord6 #eceff4;
/* Frost */
@define-color nord7 #8fbcbb;
@define-color nord8 #88c0d0;
@define-color nord9 #81a1c1;
@define-color nord10 #5e81ac;
/* Aurora */
@define-color nord11 #bf616a;
@define-color nord12 #d08770;
@define-color nord13 #ebcb8b;
@define-color nord14 #a3be8c;
@define-color nord15 #b48ead;


window {
    background-color: transparent;
}

* {
    font-family: Noto Sans;
    font-size: 14px;
    font-weight: 600;
    margin-top: 2px;
}

/*
* Left part
*/
#mode {
    color: @nord0;
    background-color: @nord13;
    margin-left: 8px;
    border-radius: 5px;
    padding: 0px 6px;
}

#workspaces button {
    margin-left: 8px;
    background-color: @nord3;
    padding: 0px 4px;
    color: @nord6;
    margin-top: 0;
}

#workspaces button.active {
    color: @nord0;
    background-color: @nord8;
}

#window {
    margin-left: 10px;
    color: @nord0;
    font-weight: bold;
    padding: 0px 5px;
}

/*
* Right part
*/
#pulseaudio,
#network,
#cpu,
#memory,
#backlight,
#language,
#battery,
#clock,
#tray {
    background-color: @nord2;
    padding: 0px 8px;
    color: @nord6;
}

#backlight,
#language,
#battery,
#clock,
#tray {
    margin-right: 8px;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}

#pulseaudio,
#network,
#clock,
#tray {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}

#pulseaudio.muted {
    background-color: @nord13;
    color: @nord3;
}

#pulseaudio, #backlight {
    background-color: @nord10;
}

#network, #cpu, #memory, #battery {
    background-color: @nord3;
}

#tray {
    background-color: @nord1;
}

#network.disabled, 
#network.disconnected {
    background-color: @nord13;
    color: @nord0; 
}

#battery.warning {
    background-color: @nord13;
    color: @nord0; 
}

#battery.critical {
    background-color: @nord11;
    color: @nord0; 
}

#tray menu {
    background-color: @nord2;
    color: @nord4;
    padding: 10px 5px;
    border: 2px solid @nord1;
}
    '';
};
  home.pointerCursor = {
    gtk.enable = true;
    # x11.enable = true;
    package = pkgs.bibata-cursors;
    name = "Bibata-Modern-Classic";
    size = 24;
  };

  gtk = {
    enable = true;
    gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
    theme = {
      package = pkgs.nordic;
      name = "Nordic";
    };

    iconTheme = {
      package = pkgs.numix-icon-theme;
      name = "Numix";
    };

    font = {
      name = "Noto Sans";
      size = 11;
    };
  };
  dconf = {
    enable = true;
    settings."org/gnome/desktop/interface".color-scheme = "prefer-dark";
  };
  qt = {
    enable = true;
    platformTheme.name = "qtct";
    style.name = "kvantum";
  };

  xdg.configFile = {
    "Kvantum/kvantum.kvconfig".text = ''
      [General]
      theme=GraphiteNordDark
    '';
    "Kvantum/GraphiteNord".source = "${pkgs.graphite-kde-theme}/share/Kvantum/GraphiteNord";
  };
}
