{config, pkgs, ...}:
{  
  #Helix
  programs.helix = {
    enable = true;
    settings = {
      theme = "nord";
      editor.cursor-shape = {
        normal = "block";
        insert = "bar";
        select = "underline";
      };
    };
  };

  # Direnv
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
    
  #GPG
  programs.gpg = {
    enable = true;
    homedir = "${config.xdg.dataHome}/gnupg";
  };
  services.gpg-agent.enable=true;
  services.gpg-agent.pinentryPackage = pkgs.pinentry-gnome3; 
  
  #Git
  programs.git = {
    enable = true;
    lfs.enable = true;
    userEmail = "dang.nq06@gmail.com";
    userName = "Quang Dang Nguyen";
    signing.signByDefault = true;
    signing.key = "0xED346FC81F14B025";
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
      push = {
        autoSetupRemote=true;
      };
    };
  };
}
