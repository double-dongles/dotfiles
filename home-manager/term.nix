{config, pkgs, ...}:
{
  #Bash autostart fish
  programs.bash = {
    enable = true;
    initExtra = ''
    if [[ $(${pkgs.procps}/bin/ps --no-header --pid=$PPID --format=comm) != "fish" && -z ''${BASH_EXECUTION_STRING} ]]
    then
	    shopt -q login_shell && LOGIN_OPTION="--login" || LOGIN_OPTION=""
	    exec ${pkgs.fish}/bin/fish $LOGIN_OPTION
    fi
    '';
  };

  #Fish terminal
  programs.fish = { 
    enable = true;
    loginShellInit = ''
      fish_add_path $HOME/.local/bin
      if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec Hyprland
      end
    '';   
    shellInitLast = '' 
      set fish_greeting
    '';
    shellAbbrs ={
      nix-shell = "nix-shell --command fish";
      cap = "sleep 1; grim";
    };
    plugins = [ {name = "hydro"; src=pkgs.fishPlugins.hydro.src;}];
  };

  # Terminal
  programs.foot = {
    enable = true;
    settings = {
      main = {
        term="xterm-256color";
        font="Cozette:size=15";
        include="${pkgs.foot.themes}/share/foot/themes/nord"; 
     };
   };
  };

  programs.nix-index.enable = true;
}
