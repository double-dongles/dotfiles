{ config, pkgs, lib, inputs, ... }:
{
  imports = [
    ./wm.nix
    ./term.nix
    ./dev.nix
  ];
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "dan";
  home.homeDirectory = "/home/dan";

  home.stateVersion = "23.11"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  nixpkgs.config.allowUnfree = true;
  home.packages = with pkgs;[
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji
    liberation_ttf
    cozette
    inputs.zen-browser.packages."${system}".twilight
    pavucontrol
    xdg-utils
    btop
    swaybg
    rclone
    unzip
    unrar
    qbitorrent
    evince
    dex
    libreoffice
    qjackctl
    vesktop
    bottles
    brightnessctl
    darktable
    kdePackages.kdenlive
    tidal-hifi
    grim
    slurp
    lutris
    protonup-qt
    protontricks
    obsidian
    vlc
    fastfetch
    wl-clipboard
    helvum
    iaito
    marksman
    prismlauncher
    mangohud
    kdePackages.xwaylandvideobridge
    limo
    p7zip
    wineWowPackages.waylandFull
    (jdk.override { enableJavaFX = true; })
    loupe
    desktop-file-utils
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    "${config.home.homeDirectory}/.local/bin" = {source = ../bin;};
  }; 
  home.sessionVariables = {
    EDITOR = "hx";
    GTK_IM_MODULE="fcitx";
    QT_IM_MODULE="fcitx";
    XMODIFIERS="@im=fcitx";
    CARGO_HOME="${config.xdg.dataHome}/cargo";
    ELECTRON_OZONE_PLATFORM_HINT="wayland";
    STEAM_FORCE_DESKTOPUI_SCALING="1.4";
  };

  dconf.enable = true;

  # XDG 
  xdg.enable = true;  
  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [xdg-desktop-portal-hyprland xdg-desktop-portal-gtk];
  };
  xdg.portal.config.common.default = "*";

  programs.obs-studio.enable=true;
  programs.obs-studio.plugins=[pkgs.obs-studio-plugins.obs-pipewire-audio-capture pkgs.obs-studio-plugins.input-overlay];

  
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
